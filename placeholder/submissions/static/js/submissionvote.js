$(document).ready(function() {

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function voteLike (submissionID) {
    debugger;
    $.ajax({
        type: "POST",
        url: "/voteLike/",
        data: {"submission": submissionID},
        success: function() {
	    window.location.reload(true);
	    //i commented out the hide arrow thing because refreshing
	    //the page removes the arrow anyway because of the if statements
	    //in the index.html template
            //$("#submission-voteLike-" + submissionID).hide();
        },
        headers: {
            'X-CSRFToken': csrftoken
        }
    });
    return false;
}

function voteDislike (submissionID) {
    debugger;
    $.ajax({
        type: "POST",
        url: "/voteDislike/",
        data: {"submission": submissionID},
        success: function() {
	    window.location.reload(true);
            //$("#submission-voteDislike-" + submissionID).hide();
        },
        headers: {
            'X-CSRFToken': csrftoken
        }
    });
    return false;
}

function voteLove (submissionID) {
    debugger;
    $.ajax({
        type: "POST",
        url: "/voteLove/",
        data: {"submission": submissionID},
        success: function() {
        window.location.reload(true);
            //$("#submission-voteLike-" + submissionID).hide();
        },
        headers: {
            'X-CSRFToken': csrftoken
        }
    });
    return false;
}

function voteHate (submissionID) {
    debugger;
    $.ajax({
        type: "POST",
        url: "/voteHate/",
        data: {"submission": submissionID},
        success: function() {
        window.location.reload(true);
            //$("#submission-voteLike-" + submissionID).hide();
        },
        headers: {
            'X-CSRFToken': csrftoken
        }
    });
    return false;
}
                  
$("a.voteLike").click(function() {
    var submissionID = parseInt(this.id.split("-")[2]);
    return voteLike(submissionID);
});

                  
$("a.voteDislike").click(function() {
    var submissionID = parseInt(this.id.split("-")[2]);
    return voteDislike(submissionID);
});

$("a.voteLove").click(function() {
    var submissionID = parseInt(this.id.split("-")[2]);
    return voteLove(submissionID);
});

$("a.voteHate").click(function() {
    var submissionID = parseInt(this.id.split("-")[2]);
    return voteHate(submissionID);
});
});
