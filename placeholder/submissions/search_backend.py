import re

from django.db.models import Q

# 'simple' search implementation I found on the internet
# (http://julienphalip.com/post/2825034077/adding-search-to-a-django-site-in-a-snap)
# may want to replace with a pre-made package such as this: http://haystacksearch.org/


def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)|(\S+)').findall,  # finds all terms that match the pattern
                    normspace=re.compile(r'\s{2,}').sub):

    # splits query string into individual keywords,
    # getting rid of unnecessary space and grouping quoted words together

    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]  # magic


def get_query(query_string, search_fields):
    # Returns a query, which is an array of Q objects.
    # that combination aims to search keywords within a model by
    # testing the given search fields

    query = None  # query to search for every search term
    terms = normalize_query(query_string)
    for term in terms:
        or_query=None  # query to search for a give term i each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term}) # get query for this term
            if or_query is None:
                or_query = q # initialise or_query
            else:
                or_query = or_query | q # make sure the query is not added twice?
        if query is None:
            query = or_query # initialise query
        else:
            query = query & or_query # add new members of or_query to query?

    return query
