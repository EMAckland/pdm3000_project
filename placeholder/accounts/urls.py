from django.conf.urls import url
from . import views
from django.contrib.auth import views as django_views #currently using default django views for login and logout, might want to change eventually, or like, effort -emily




app_name = 'accounts'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^login/$', django_views.login, name='login',
        kwargs={'template_name': 'accounts/login.html'}),
    url(r'^logout/$', django_views.logout, {'next_page': '/'}), #redirects home page on logout
    url(r'^editprofile/$', views.editProfile, name='editProfile'),
    url(r'^changepassword/$', views.changePassword),
]
