from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
# Create your models here.


class UserProfile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	department = models.CharField(max_length=100)
	GENDER_CHOICES = (('M', 'Male'),('F', 'Female'),('N/A', 'N/A'),)
	gender = models.CharField(max_length=3, choices=GENDER_CHOICES, default='N/A')
	points = models.IntegerField(default=1)

def create_profile(sender, **kwargs):
	user = kwargs["instance"]
	if kwargs["created"]:
		user_profile = UserProfile(user=user)
		user_profile.save()
post_save.connect(create_profile, sender=User)



