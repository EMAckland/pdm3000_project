from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import UserProfile
from .forms import UserForm
from django.forms.models import inlineformset_factory
from django.core.exceptions import PermissionDenied
from django.contrib.auth import update_session_auth_hash


# Create your views here.
@login_required() 
def index(request):
    user = request.user
    username = user.username
    name = str(user.first_name) + " " + str(user.last_name)
    email = user.email
    department = UserProfile(user).department
    print department

    return render(request, 'accounts/index.html', {
    'user' : request.user,
    'username' : user.username,
    'email' : user.email,
    'name' : name,
    'department' : department,
        })


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect('/')
    else:
        form = UserCreationForm()
 
    return render(request, 'accounts/signup.html', {
        'form': form,
        })

def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            render(request, 'accounts/success.html')
        else:
            render(request, 'accounts/failed_disabled.html')
    else:
        render(request, 'accounts/failed_invalid.html')

@login_required() 
def editProfile(request):

    user = request.user

    user_form = UserForm(instance=user)
 
    ProfileInlineFormset = inlineformset_factory(User, UserProfile, fields=('department', 'gender'))
    formset = ProfileInlineFormset(instance=user)

    if request.user.is_authenticated() and request.user.id == user.id:
        if request.method == "POST":
            user_form = UserForm(request.POST, request.FILES, instance=user)
            formset = ProfileInlineFormset(request.POST, request.FILES, instance=user)
 
            if user_form.is_valid():
                created_user = user_form.save(commit=False)
                formset = ProfileInlineFormset(request.POST, request.FILES, instance=created_user)
 
                if formset.is_valid():
                    created_user.save()
                    formset.save()
                    return HttpResponseRedirect('/accounts/')
 
        return render(request, "accounts/editProfile.html", {
            "user": user,
            "user_form": user_form,
            "formset": formset,
        })
    else:
        raise PermissionDenied

@login_required
def changePassword(request):
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return HttpResponseRedirect('/accounts/')
    else:
        form = PasswordChangeForm(user=request.user)
    return render(request, "accounts/password_change.html", {
        'form': form,
    })