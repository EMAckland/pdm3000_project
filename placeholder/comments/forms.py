from django.forms import ModelForm
from comments.models import Comment
from django.db import models
from django.contrib.auth.models import User
from submissions.models import Submission

class CommentForm(ModelForm):
	class Meta:
		model = Comment
		fields = ['text',]

class Comment(object):
	text = models.TextField(null=True, blank=False, max_length=5000 )


