from __future__ import unicode_literals

from django.db import models
from submissions.models import Submission

from django.contrib.auth.models import User

class Comment(models.Model):
#	submission = models.ForeignKey(SubmissionThread, related_name='submission_thread')
	text = models.TextField(null=True, blank=False, max_length=5000 )
	points = models.IntegerField(default=1)
	author = models.ForeignKey(User, related_name='moderated_comments')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	submission = models.ForeignKey(Submission, related_name='submission_thread')
	commentsLikes = models.ManyToManyField(User, related_name='liked_comments')
	commentsDislikes = models.ManyToManyField(User, related_name='disliked_comments')
	commentsLoves = models.ManyToManyField(User, related_name='loved_comments')
	commentsHates = models.ManyToManyField(User, related_name='hated_comments')

def __unicode__(self):
	 return (self.submission)

def get_absolute_url(self):
	return "/%s/comments" % (submission.id)