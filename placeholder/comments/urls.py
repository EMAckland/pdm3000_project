from django.conf.urls import url
import comments.views

urlpatterns = [
	url(r'^$', comments.views.index),
	url(r'^comment', comments.views.comment),
	url(r'^voteDislike', comments.views.voteDislike),
	url(r'^voteLike', comments.views.voteLike),
	url(r'^voteLove', comments.views.voteLove),
	url(r'^voteHate', comments.views.voteHate),
]